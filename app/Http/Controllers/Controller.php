<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

     /**
     * Initialize api required parameters
     */
    public function send($method, $urlParam=null, $data = array(), $filePath='')
    {  
        $data = is_array($data) ? $data : '';

        try {  
        	// $baseUrl = "https://api.bildarb.com/api/";
        	// $arrGuzzleParams = ['base_url' => [$baseUrl, []],];
        	$client = new \GuzzleHttp\Client();

            if ($data) 
            {   
                $response = $client->request($method, $urlParam, ['headers' => $headers, 'json' => $data]);
            } 
            else 
            {
                $response = $client->request($method, $urlParam );
            }

            $response = $response->getBody();

            $response = json_decode($response, true);
         } 
         catch (Guzzle\Http\Exception\BadResponseException $e) 
         {
             $res=array();
             $res['status'] = 0;
             $res['errorCode'] = $e->getCode();
             $res['errorMessage'] = $e->getMessage();
             $res['HTTP request URL'] =  $e->getRequest()->getUrl();
             $res['HTTP request'] =  $e->getRequest(); 
             $res['HTTP response status'] =$e->getResponse()->getStatusCode();
             $res['HTTP response'] =$e->getResponse();
             echo json_encode($res);die;
        } 
        catch (Guzzle\Http\Exception\RequestException $e) 
        {
            $res['status'] = 0;
            $res['errorCode'] = $e->getCode();
            $res['errorMessage'] = $e->getMessage();
            echo json_encode($res); die;
        
        } 
        catch (GuzzleHttp\Exception\ClientException $e) 
        {
             $res['status'] = 0;
             $res['errorCode'] = $e->getCode();
             $res['errorMessage'] = $e->getMessage();
            echo json_encode($res); die;
        }
       /*
            $res->json();
            $res->getBody();
            $res->getStatusCode();
            $res->getHeader('content-type');
        */
        return $response;
    }
}
