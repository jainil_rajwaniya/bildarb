<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

class HomeController extends Controller
{
    //

    public function index(Request $request) {
    	//return view('user.profile', ['user' => User::findOrFail($id)]);
    	//die('fdsfadsfsdfasd');

    	$metaArr['title'] = 'Index-Title';
    	$metaArr['keywords'] = 'Index-Keywords';
    	$metaArr['description'] = 'Index-Description';

    	if('ar' == Config::get('app.locale')) {
    		return view('layouts.home.ar.index', ['metaArr' => $metaArr]);
    	} else {
    		return view('layouts.home.en.index', ['metaArr' => $metaArr]);
    	}
    }
}
