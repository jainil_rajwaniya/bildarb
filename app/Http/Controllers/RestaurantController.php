<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;

class RestaurantController extends Controller
{

    public function index(Request $request) {
        //$this->middleware('auth');

    	//return view('user.profile', ['user' => User::findOrFail($id)]);
    	//die('fdsfadsfsdfasd');
    	$metaArr['title'] = 'Restaurant';
    	$metaArr['keywords'] = 'RestaurantRestaurant';
    	$metaArr['description'] = 'RestaurantRestaurantRestaurantRestaurantRestaurant';

        // \Config::get('app.locale');


        // $restaurantsApiUrl = \Config::get('app.api_base_uri') . \Config::get('api_uri.ALL_RESTAURANTS');
        // $response = $this->send('get', $restaurantsApiUrl, '');

        if('ar' == Config::get('app.locale')) {
            return view('layouts.restaurant.ar.index', ['metaArr' => $metaArr]);
        } else {
            return view('layouts.restaurant.en.index', ['metaArr' => $metaArr]);    
        }
    }
}
