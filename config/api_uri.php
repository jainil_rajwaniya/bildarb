<?php

return [
	//===================COMMON=====================
	"LOCALDATENTIME" => "account/localdatentime", //account/localdatentime
	

	//===================HOME PAGE==================
	"ALL_RESTAURANTS" => "restaurant/getweb?",
	"BRANCHES_BY_RESTAURANT_ID" => "restaurant/branch/", //restaurant/branch/12

	//===================BOOK TABLE==================
	"RESTAURANT_BY_ID" => "restaurant/", //restaurant/12

	//===================CART==================
	"PENDING_USER_CART" => "shoppingcart/pending/", //shoppingcart/pending/3/34/2
	"ADD_ITEM_IN_CART" => "shoppingcart/allitemandsubitem", //POST

	//===================VIEW CART==================
	"PROMO_DISCOUNT" => "shoppingcart/removepromodiscount/", //shoppingcart/removepromodiscount/3/34/2
	"RESERVATION" => "reservation/guestlist/", //reservation/guestlist/0/8.000


	//===================RESTAURANT SEARCH==================
	"RESTAURANT_BY_NAME" => "restaurant/restbyname/", //restaurant/restbyname/prime/2
	"FILTER_RESTAURANTS" => "restaurant/filter", //POST
	

];