@extends('layouts.master')

@section('metaTags')
	@include('layouts.common.meta', ['metaArr' => $metaArr])
@endsection

@section('pageAssets')
<style>
.menuCenterClass li:nth-child(2) a font{border-bottom: 2px solid #fc204e;}

.videoBlock {
   background-color: #ddd;
   display: block;
   position: relative;
   padding: 0 0 56.25% 0;
}
.videoBlock .video {
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
}


#video {
  transition: width .2s ease-in-out, height .2s ease-in-out, transform .38s ease-in-out;
}
/** Use .sticky */
#video.is-sticky {
background-color:white;
border-radius: 2px;
bottom: 20px;
box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.25);
left: 20px;
padding: 5px 0px 0px 0px;
position: fixed;
width: 300px;
z-index: 9999;
height: 180px !important;
}

</style>
        
@endsection

@section('content')
    <div class="hero" id="about_video">
        <div id="order-page-title" class="page-title-style2"  >
            <div id="videoDiv"> 
                <div id="videoBlock" class="">   
                    <video class="video" preload="preload"  id="video" autoplay="autoplay" loop="loop" controls>
                        <source src="error/error404e1a4.html" type="video/webm"></source>
                        <source src="images/Video%20Oct%2005%2c%202%2059%2011%20PM.mp4" type="video/mp4"></source>
                    </video>
                    <!-- <div>
                        <img id="pauseThisVid" src="images/pause.png" style="display:inline-block" onclick="pauseVideoClick();"/>
                        <img id="playThisVid" src="images/play.png" style="display:none;" onclick="playVideoClick();"/>
                        &nbsp;&nbsp;&nbsp;
                        <img id="muteThisVid" src="images/unmute.png" style="display:inline-block" onclick="unmuteVideo();"/>
                        <img id="unmuteThisVid" src="images/mute.png" style="display:none;" onclick="muteVideo();"/>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="searchquick2">
            <div class="container">
                    <img src="images/icon-network-about.png" class="about_bildarb">
                    <hr class="hrStyleCss">
                    <h2 class="logo-content regularweb">About Bildarb</h2>
                    <p class="logo-subtitle">Bildarb is Kuwait's premier hospitality technology platform, helping restaurants and guests connect. Bildarb helps guests discover restaurants, book reservations, get personalized service and pick up orders.</p>
                    <p class="logo-subtitle">Bildarb app will be helping the restaurants in getting better CRM and receive actionable data that helps them better understand their business.</p>
                    <hr class="hrStyleCss">
                    <img src="images/APP-STORE.jpg"  class="app_store_img">
            </div>
    </div>
<script type="text/javascript" src="js/custom-js/about.js"></script>
@stop
