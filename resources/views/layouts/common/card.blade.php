<?php
$rand = rand(1,9);
?>

<div class="team-item col-xs-12 col-sm-6 col-md-4 wow bounceInUp boxlay4 animated">
    <div class="team-item-block">
        <div class="team-thumb" style="">
            <img src="images/demo/<?= $rand.'.jpg' ?>">
            <div class="team-overlay">
                <div class="row">
                    <div>
                        <button type="button" class="btn btn-link btn-sm orderLtr opened_color"> Opened</button>
                    </div>
                    <div>
                        <a class="btn btn-sm btn-link orderLtr btn-image btnPickup1">
                        12:00 PM - 11:00 PM
                        <br>
                    </a>
                    </div>
                    <div class="text-center"></div>
                    <div class="wow zoomIn animated animated">Peacock</div>
                    <div class="text-center item_bottom_block">
                        <a href="order.php" class="btn btn-primary btn-image btnPickup wow bounceInLeft borderRadius animated">Pickup</a>&nbsp;
                        <a href="book-table.php" class="btn btn-primary btn-image btnReservation wow bounceInRight borderRadius animated">Reservation</a>
                        <div class="dropdown">
                            <button class="btn btn-default dropdown-toggle langDd safariBrowserlangDd" type="button" id="langDd" data-toggle="dropdown"><img src="images/icns/btns.png"></button>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">0</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">1</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cover coverboxx"></div>
                <div>
                </div>
            </div>
			<div class="card_social_icons">
				<ul class="toplist">
					<li><a href=""><i class="fa fa-facebook"></i></a></li>
					<li><a href=""><i class="fa fa-twitter"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin"></i></a></li>
					<li class="last"><a href=""><i class="fa fa-heart red"></i></a></li>
				</ul>
			</div>
        </div>
    </div>
</div>