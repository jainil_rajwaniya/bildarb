<div class="clearfix"></div>
            <!--Download App End -->
            <script src="scripts/getArea.js"></script>
            <div class="clearfix"></div>
            <!-- start footer -->
            <footer class="footer-wrapper-area fixedFooter">
                <!-- start footer area -->
                <div class="footer-area-content">
                    <!-- Newsletter -->
                    <div id="footer-newsletter">
                    </div>
                    <!-- END: Newsletter -->
                    <div class="container">
                        <div class="footer-wrapper style-3">
                            <footer class="type-1">
                                <div class="footer-columns-entry">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Popular cities</h3>
                                            <ul class="column">
                                                <li><a href="#" class="footerATag">Al Asimah</a></li>
                                                <li><a href="#" class="footerATag">Hawalli</a></li>
                                                <li><a href="#" class="footerATag">Kuwait City</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Bildarb</h3>
                                            <ul class="column">
                                                <li><a href="#" class="footerATag">About Us</a></li>
                                                <li><a href="#" class="footerATag" href="">Contact Us</a></li>
                                                <li><a href="#" class="footerATag">Terms and conditions</a></li>
                                                <li><a href="" target="_blank" class="footerATag">Bildarb for restaurants</a></li>

                                            </ul>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <h3 class="column-title">Download bildarb app</h3>
                                            <ul class="column">
                                                <li>
                                                    <a target="_blank" href="https://itunes.apple.com/us/app/bildarb/id1349443656?ls=1&amp;mt=8" class="footerATag btn-store btn-appstore">
                                                        <img src="images/app-store.png" style="width:160px" />
                                                    </a>
                                                </li>
                                                <li>
                                                    <a target="_blank" href="https://play.google.com/store/apps/details?id=bildarb.desind.com" class="footerATag ">
                                                        <img src="images/google-play.png" style="width:160px" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
                <!-- footer area end -->

                <div class="bottom-footer">

                    <div class="container">

                        <div class="row">

                            <div class="col-sm-4 col-md-4">

                                <p class="copy-right">&#169; Copyright 2018 <span>bildarb</span></p>

                            </div>

                            <div class="col-sm-4 col-md-4">

                            </div>

                            <div class="col-sm-4 col-md-4">
                                <ul class="bottom-footer-menu for-social for_social_icons">
                                    <li><a href="https://twitter.com/bildarb"><i class="ri ri-twitter" data-toggle="tooltip" data-placement="top" title="twitter"></i></a></li>
                                    <li><a href="https://www.facebook.com/bildarb"><i class="ri ri-facebook" data-toggle="tooltip" data-placement="top" title="facebook"></i></a></li>
                                    <li><a href="https://www.linkedin.com/company/bildarb-app"><i class="fa fa-linkedin" data-toggle="tooltip" data-placement="top" title="linkedin"></i></a></li>
                                    <li><a href="https://www.youtube.com/channel/UCUmjBDXm-Yq8vkpUtfjd36A"><i class="ri ri-youtube-play" data-toggle="tooltip" data-placement="top" title="youtube"></i></a></li>
                                    <li><a href="https://www.instagram.com/bildarb/"><i class="fa fa-instagram" data-toggle="tooltip" data-placement="top" title="instagram"></i></a></li>
                                    <li><a href="https://in.pinterest.com/bildarbapp/"><i class="fa fa-pinterest" data-toggle="tooltip" data-placement="top" title="pinterest"></i></a></li>
                                </ul>
                            </div>

                        </div>
                    </div>

                </div>

            </footer>
            <!-- end footer -->

        </div>

    </div>

    <!-- start Main Wrapper -->

    <div>

        <!-- footer area end -->
        <!-- end footer -->
    </div>
        <!---->