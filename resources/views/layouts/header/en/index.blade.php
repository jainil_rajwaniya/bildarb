<!-- start Header -->
<header id="header">
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="topnavbar">
            <ul class="navrightl">
                <li><a href=""><i class="fa fa-mobile"></i> Get The <span>App</span></a></li>
            </ul>
            <ul class="navrightb">
                <li class="borright"><a href="#" data-toggle="modal" data-target="#myModal22" onclick="ad()">Login</a></li>
                <li><a href="#" data-toggle="modal" data-target="#myModal22" onclick="af()">Register</a></li>
                <li class="safariBrowserLanDd">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle langDd safariBrowserlangDd language_dropdown_btn" type="button" id="langDd" data-toggle="dropdown">EN<span class="caret"></span></button>
                        <ul class="dropdown-menu language_dropdown" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="" class="language_">English</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="" onclick="" class="language_">عربي</a></li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
        <div class="container">
            <div class="navbar-header">
                <button button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}"><img class="logoWidth" src="images/logo.png" alt="Logo" /></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav menuCenterClass">
                    <li>
                        <a href="{{ url('/') }}"><font style="text-transform:uppercase">H</font>ome</a>
                    </li>
                    <li>
                        <a href="{{ url('aboutus') }}"><font style="text-transform:uppercase">A</font>bout</a>
                    </li>
                    <li>
                        <a href="#"><font class="uppercase">A</font>dd restaurant</a>
                    </li>
                </ul>

            </div>
        </div>
    </div>
</header>
