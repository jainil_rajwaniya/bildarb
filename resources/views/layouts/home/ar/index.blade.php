@extends('layouts.master')

@section('metaTags')
	@include('layouts.common.meta')
@endsection

   @if ( Config::get('app.locale') == 'en')

   {{ 'Current Language is English' }}

   @elseif ( Config::get('app.locale') == 'ar' )

   {{ 'Current Language is Arabic' }}

   @endif

@section('pageAssets')
    <link rel="stylesheet" href="path/to/asset.css">
@endsection

@section('content')
    <p>This is my body content.</p>
@stop