@extends('layouts.master')

@section('metaTags')
	@include('layouts.common.meta')
@endsection

   @if ( Config::get('app.locale') == 'en')


   @elseif ( Config::get('app.locale') == 'ar' )


   @endif

@section('pageAssets')
    <!--<link rel="stylesheet" href="path/to/asset.css">-->
@endsection

@section('content')
    <div class="hero banner_hero">
                <div class="container">
                    <img src="images/midimg.png" class="autod" />
                    <h1 class="bannerTxtFontSize regularweb" style=""> Find The Best Restaurant, Cafes And Bars<!--<span onclick="openNav()" id="areaDdSpan" class="current-metro ng-binding" style="border-bottom: 1px solid #fff;cursor: pointer;white-space: nowrap;font-weight: 600;-webkit-tap-highlight-color: rgba(0,0,0,.2);" > <span class="" style="display: inline-block; width: 0; height: 0; border-left: 7px solid transparent; border-right: 7px solid transparent; border-top: 7px solid #fff; vertical-align: middle;" > </span > </span >--></h1>
                    <div class="searchbarb">
                        <div class="s003">
                            <form>
                                <div class="inner-form" id="searchbarff">
                                    <div class="input-field first-wrap">
                                        <div class="input-select">
                                            <select data-trigger="" name="choices-single-defaul">
                                                <option placeholder="">City</option>
                                                <option>Kuwait City</option>
                                                <option>Al Asimah</option>
                                                <option>Hawalii</option>
                                            </select>
                                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                                        </div>
                                    </div>
                                    <div class="input-field second-wrap">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        <input id="search" type="text" placeholder="Search for restaurants or cuisines" />
                                    </div>
                                    <div class="input-field third-wrap">
                                        <button onclick="location.href='/restaurant'" class="btn-search" type="button">
                                            Search
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="cover home_overlay"></div>
            </div>
            <!-- end hero-header -->

            <!-- start order process -->
            <!-- <div class="order-process-step bg-light pb-80 pt-80" > <div class="container frontend" > <div class="clearfix" > </div > <div class="block process-block" > <div class="row text-center" > <ol class="process" > <li class="col-md-3 col-sm-6 col-xs-6 wow bounceInDown" > <img src="~/images/icns/red/home1.png" style="margin-top: 20%;margin-left: 36%;height: 59px;width: 68px;margin-right: 30%;" / > <h4 style="margin-bottom: 10%;margin-top: 22%;" > Pick a restaurant</h4 > </li > <li class="col-md-3 col-sm-6 col-xs-6 lnr-text-align-center wow bounceInUp" > <img src="~/images/icns/red/reservation.png" style="margin-top: 20%;margin-left: 36%;height: 57px;width: 77px;margin-right: 30%;" aria-hidden="true" / > <h4 style="margin-bottom: 10%;margin-top: 22%;" > Book a Table</h4 > </li > <li class="col-md-3 col-sm-6 col-xs-6 wow bounceInDown" > <img src="~/images/icns/red/preorder.png" style="margin-top: 20%;margin-left: 36%;height: 70px;width: 68px;margin-right: 30%;" / > <h4 style="margin-bottom: 10%;margin-top: 16%;" > Make Pre Order or Pick up Order</h4 > </li > <li class="col-md-3 col-sm-6 col-xs-6 wow bounceInUp" > <img src="~/images/icns/red/rating.png" style="margin-top: 20%;margin-left: 36%;height: 50px;width: 59px;margin-right: 39%;" / > <h4 style="margin-bottom: 10%;margin-top: 26%;" > Happy, enjoy</h4 > </li > </ol > </div > <div class="clearfix" > </div > </div > <div class="clearfix" > </div > </div > </div >-->
            <!-- end order process -->
            <div class="pt-40">
                <div class="container">
                    <div class="wow bounceInDown">
                        <h2 class="logo-content regularweb">Trending Restaurants</h2>
                        <h4 class="logo-subtitle marbott regularweb1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                        @for ($i = 0; $i < 9; $i++)
                            @include('layouts.common.card')
                        @endfor
                    </div>
                    <center>
                        <input onclick="location.href='listing.php'" type="button" class="borderBtn" value="View all Restaurants" />
                    </center>
                    <hr class="hrStyleCss">
                </div>
            </div>
            <div class="searchquick pad_bot_zero">
                <div class="container">
                    <h2 class="logo-content regularweb">Quick search</h2>
                    <h4 class="logo-subtitle regularweb1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                    <div class="row">
                        <div class="hvr-float-shadow">
                            <img src="images/icns/1.png" />
                            <p>Cafes</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/2.png" />
                            <p>Lunch</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/3.png" />
                            <p>Dinner</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/4.png" />
                            <p>Fast food</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/5.png" />
                            <p>Breakfast</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/6.png" />
                            <p>Beverages</p>
                        </div>
                        <div class="hvr-float-shadow">
                            <img src="images/icns/7.png" />
                            <p>Desserts & Bakery</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="searchquick1 pad_bot_zero">
                <div class="container">
                    <h2 class="logo-content regularweb">Browse By Area</h2>
                    <h4 class="logo-subtitle regularweb1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                    <div class="row">
                        <div class="col-md-4 hvr-grow-shadow">
                            <img src="images/alasimah.jpg" />
                            <div class="overlay1">
                                <p>Al Asimah
                                    <br/>2 Restaurants</p>
                            </div>
                        </div>
                        <div class="col-md-4 hvr-grow-shadow">
                            <img src="images/hawalli.jpg" />
                            <div class="overlay1">
                                <p>Hawalli
                                    <br/>2 Restaurants</p>
                            </div>
                        </div>
                        <div class="col-md-4 hvr-grow-shadow">
                            <img src="images/kuwaitcity.jpg" />
                            <div class="overlay1">
                                <p>Kuwait City
                                    <br/>2 Restaurants</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="download-app-area pad_bot_zero email_box_layout">
                <div class="download-app-sec" style="">
                    <div class="mask">
                        <div class="container">
                            <div class="col-lg-7 col-md-7 col-sm-12 container-cell left-container">
                                <div class="app-content row">
                                    <div class="inner btngrp">
                                        <h2 class="logo-content getthe">Get the <span>App</span></h2>
                                        <h4 class="logo-subtitle getthe">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</h4>
                                        <div class="input-group inp1">
                                            <input type="text" class="form-control" placeholder="Enter Your Email Id">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    Email me a link
                                                </button>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-4 samebor">

                                            </div>
                                            <div class="col-md-2 smaeorb">
                                                <h5>Or</h5>
                                            </div>
                                            <div class="col-md-4 samebor">

                                            </div>
                                        </div>
                                        <div class="input-group inp2">
                                            <input type="text" class="form-control" placeholder="Enter Your Phone Number">
                                            <div class="input-group-btn">
                                                <button class="btn btn-default" type="submit">
                                                    Text me a link
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 right-align">
                                <div class="left-area">
                                    <img src="images/home-phone-apps.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@stop