<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Bildarb</title>
        @yield('metaTags')
        @yield('pageAssets')
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link rel="shortcut icon" href="images/favicon.ico" />
        
        <link href="css/icon.css" rel="stylesheet" />

        <link href="css/main.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet" />
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <!-- CSS Custom -->
        <link href="css/style.css" rel="stylesheet">
        
        <!-- CSS Font Icons -->
        <link rel="stylesheet" href="icons/linearicons/style.css">
        <link rel="stylesheet" href="icons/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="icons/simple-line-icons/css/simple-line-icons.css">
        <link rel="stylesheet" href="icons/ionicons/css/ionicons.css">
        <link rel="stylesheet" href="icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
        <link rel="stylesheet" href="icons/rivolicons/style.css">
        <link rel="stylesheet" href="icons/flaticon-line-icon-set/flaticon-line-icon-set.css">
        <link rel="stylesheet" href="icons/flaticon-streamline-outline/flaticon-streamline-outline.css">
        <link rel="stylesheet" href="icons/flaticon-thick-icons/flaticon-thick.css">
        <link rel="stylesheet" href="icons/flaticon-ventures/flaticon-ventures.css">
        <link rel="stylesheet" href="css/sweetalert2.css">
        
        <script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script defer src="js/latlon-spherical.js"></script>
        <script defer src="js/dms.js"></script>
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOTK1eZJnW0uUoV-ibeT810Z2EfSL3PBs" async defer></script>-->
        <script src="js/sweetalert2.js"></script>
        
    </head>
    <body class="not-transparent-header">
    <div id="mybannerApp"></div>
    <div class="container-wrapper">
       <!-- header starts-->
        @if ( Config::get('app.locale') == 'ar')
            @include('layouts.header.ar.index')
        @else
            @include('layouts.header.en.index')
        @endif
        <!-- header ends -->
       
        <!-- page content starts -->
        @yield('content')
        <!-- page content ends -->
        
        <!-- footer starts-->
        @if ( Config::get('app.locale') == 'ar')
            @include('layouts.footer.ar.index')
        @else
            @include('layouts.footer.en.index')
        @endif
        <!-- footer ends -->
    </body>
    <script>
    function showmsg(type, icon, title, msg) {
        $.notify({
            title: title,
            icon: icon,
            message: msg
        }, {
            type: type,
            //delay: 10000,
            //timer: 1000,
            animate: {
                enter: 'animated fadeInUp',
                exit: 'animated fadeOutRight'
            },
            placement: {
                from: "top",
                align: "right"
            },

            offset: 20,
            spacing: 10,
            z_index: 1031,
        });
    }

    $(function() {
        localStorage.setItem("lang", "English");
        $("#ref").hover(function() {
            animate(".demo", 'flash');
            return false;
        });
    });

    function animate(element_ID, animation) {
        $(element_ID).addClass(animation);
        var wait = window.setTimeout(function() {
            $(element_ID).removeClass(animation)
        }, 1300);
    }

    function ad() {
        $("#lcp1").click();
    }

    function af() {

        $("#rcp1").click();
    }

    function returnday(number) {
        var weekday = new Array(7);
        weekday[1] = "Monday";
        weekday[2] = "Tuesday";
        weekday[3] = "Wednesday";
        weekday[4] = "Thursday";
        weekday[5] = "Friday";
        weekday[6] = "Saturday";
        weekday[7] = "Sunday";

        return weekday[number];

    }
</script>

<!-- JS -->
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.waypoints.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script>
    new WOW().init();
</script>
<!--<script src="js/bootstrap.js"></script>-->
<!--<script src="js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="js/jquery.slicknav.min.js"></script>
<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
<script type="text/javascript" src="js/bootstrap-tokenfield.js"></script>
<script type="text/javascript" src="js/typeahead.bundle.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="js/jquery-filestyle.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/ion.rangeSlider.min.js"></script>
<script type="text/javascript" src="js/handlebars.min.js"></script>
<script type="text/javascript" src="js/jquery.countimator.js"></script>
<script type="text/javascript" src="js/jquery.countimator.wheel.js"></script>
<script type="text/javascript" src="js/slick.min.js"></script>
<script type="text/javascript" src="js/easy-ticker.js"></script>
<script type="text/javascript" src="js/jquery.introLoader.min.js"></script>
<script type="text/javascript" src="js/jquery.responsivegrid.js"></script>
<script type="text/javascript" src="js/customs.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<!--<script src="js/bootstrap-notify.min.js"></script>
<script src="js/bootstrap-notify.js"></script>
<script src="js/profile/lg1.js"></script>-->
<script src="js/choices.js"></script>
<script>
    const choices = new Choices('[data-trigger]', {
        searchEnabled: false,
        itemSelectText: ''
    });
</script>
<!--    <script type="text/javascript">
        var locations = [
          ['Bondi Beach', -33.890542, 151.274856, 4],
          ['Coogee Beach', -33.923036, 151.259052, 5],
          ['Cronulla Beach', -34.028249, 151.157507, 3],
          ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
          ['Maroubra Beach', -33.950198, 151.259302, 1]
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 10,
          center: new google.maps.LatLng(-33.92, 151.25),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
        </script>-->
</html>