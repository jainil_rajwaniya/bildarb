@extends('layouts.master')

@section('metaTags')
	@include('layouts.common.meta', ['metaArr' => $metaArr])
@endsection

@section('pageAssets')
    <link rel="stylesheet" href="path/to/asset.css">
@endsection

@section('content')
    <p>This is my Restaurant content.</p>
@stop