<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::group(array('domain' => 'restaurant.bookmytable.com'), function () {
//  	Route::get('/', 'UserController@index');
// });

// Route::get('/{name?}', function ($name = null) {
//     return view('welcome');
// })->where('name', '(home)');

// Route::get('/{name?}', 'HomeController@index')->where('name', '(home)');

// Route::get('/kuwait/restaurant/', 'RestaurantController@index');


// Route::group(array('domain' => '{subdomain}.bookmytable.com'), function () {
 
//     Route::get('/', function ($subdomain) {
 
//         $name = DB::table('users')->where('name', $subdomain)->get();
 
//         dd($name);
 
//     });
// });


$locale = Request::segment(1);

if (in_array($locale, \Config::get('app.available_locales'))) {
    \Config::set('app.locale', $locale);
} else {
    $locale = null;
}

Route::group(array('prefix' => $locale), function()
{
	Route::get('/aboutus', 'AboutusController@index');
	Route::get('/booktable', 'BooktableController@index');
	Route::get('/cart', 'CartController@index');
	Route::get('/contactus', 'ContactusController@index');
	Route::get('/{name?}', 'HomeController@index')->where('name', '(home)');
	Route::get('/profile', 'ProfileController@index');
	Route::get('/restaurant', 'RestaurantController@index');	
	Route::get('/termsandconditions', 'TermsandconditionsController@index');
	Route::get('/viewcart', 'ViewcartController@index');
});